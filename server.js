//========================== Imports ============================

const express = require("express");
const session = require("express-session");
const dotenv = require("dotenv").config();
const cookie = require("cookie");
const crypto = require("crypto");
const queryString = require("querystring");
const request = require("request-promise");
const nonce = require("nonce")();

// ================ code =========================================

const PORT = process.env.PORT || 3000;
const apiKey = process.env.SHOPIFY_API_KEY;
const apisecret = process.env.SHOPIFY_API_SECRET;
const appurl = process.env.SHOPIFY_APP_URL;
const scope = ["read_products", "write_products"];
const forwardingAddress = appurl;

const app = express();

/* this route generates state code stores it using cookie and redirects to Install Url*/

app.get("/shopify", (req, res) => {
	//const shop = req.query.shop;

	/* need to move this to .env file after done with basic functionality */
	const shop = "shah-nidhi.myshopify.com";

	if (shop) {
		const state = nonce();
		const redirectUri = forwardingAddress + "/shopify/callback";
		const installUrl =
			"https://" +
			shop +
			"/admin/oauth/authorize?client_id=" +
			apiKey +
			"&scope=" +
			scope +
			"&state=" +
			state +
			"&redirect_uri=" +
			redirectUri;

		res.cookie("state", state);
		res.redirect(installUrl);
	} else {
		return res.status(400).send("Missing shop parameter !"); //no query parameter
	}
});

/* Install Url get redirected and this route gets called with some query parameter and generates
   access token to fetch the authenticated data from Shopify json files */

app.get("/shopify/callback", (req, res) => {
	const { shop, hmac, code, state } = req.query;

	const stateCookie = cookie.parse(req.headers.cookie).state;
	if (state !== stateCookie) {
		return res.status(400).send("Request Origin cannot be verified !");
	}

	if (shop && hmac && code) {
		const map = Object.assign({}, req.query);
		delete map["hmac"]; //we cant hash with the same hmac parameter provided in it same like JWT
		const message = queryString.stringify(map);
		const generateHash = crypto
			.createHmac("sha256", apisecret)
			.update(message)
			.digest("hex");

		if (generateHash != hmac) {
			return res
				.status(400)
				.send("HMAC validation failed !"); /* doesnt match */
		}

		const accessTokenRequestUrl =
			"https://" + shop + "/admin/oauth/access_token";
		const accessTokenPayload = {
			client_id: apiKey,
			client_secret: apisecret,
			code,
		};

		/* Requests to the shop for access token using secret key & apikey */
		request
			.post(accessTokenRequestUrl, { json: accessTokenPayload })
			.then(accessTokenResponse => {
				const accessToken = accessTokenResponse.access_token;

				const apiRequestUrl = "https://" + shop + "/admin/products.json";
				const apiRequestHeader = {
					"X-Shopify-Access-Token": accessToken,
				};

				request
					.get(apiRequestUrl, { headers: apiRequestHeader })
					.then(apiResponse => {
						res.end(apiResponse);
					})
					.catch(error => {
						res.status(error.statusCode).send(error.error.error_description);
					});
			})
			.catch(error => {
				res.status(error.statusCode).send(error.error.error_description);
			});
	} else {
		res.status(400).send("Required parameter missing !");
	}
});

app.listen(PORT, () => {
	console.log("Server is up and running on port : " + PORT);
});
